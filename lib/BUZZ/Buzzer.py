#!/usr/bin/env python
# -*- coding: utf8 -*-

import gpio as GPIO   #import the GPIO library
import time               #import the time library
import logging

class Buzzer(object):
    def __init__(self,pin=203):
        self.buzzer_pin = pin
        GPIO.setup(self.buzzer_pin, GPIO.OUT)
        GPIO.set(self.buzzer_pin, True) #TRUE is buzzer off
        logging.getLogger("gpio").setLevel(logging.ERROR)
        print("buzzer ready")

    def __del__(self):
        class_name = self.__class__.__name__
        print (class_name, "finished")

    def buzz(self, duration):   #create the function “buzz” and feed it the pitch and duration)
        state=True
        for delay in duration:
            #print("State ="+str(state))
            #print("Delay ="+str(delay))
            if(state):
                GPIO.set(self.buzzer_pin, False)   #False is buzzer on
                time.sleep(delay)
                GPIO.set(self.buzzer_pin, True)    #True is buzzer off
                state=False
            else:
                state=True
                time.sleep(delay)
        GPIO.set(self.buzzer_pin, True) #False is buzzer on

    def play(self, tune):
        x=0

        #Sucsess Sound
        print("Playing tune ",tune)
        if(tune==1):
            duration=[0.15,0.1,0.15]
            self.buzz(duration)

        #Error Sound
        elif(tune==2):
            duration=[0.15,0.1,0.15,0.1,0.15]
            self.buzz(duration)

        #Startup Sound       
        if(tune==3):
            duration=[0.15,0.2,0.15,0.1,0.15]
            self.buzz(duration)  #feed the pitch and duration to the function, “buzz”
                
        elif(tune==4):
            duration=[0.15,0.1,0.20,0.1,0.25]
            self.buzz(duration)

if __name__ == "__main__":
    buzzer = Buzzer(203)
    buzzer.play(4)