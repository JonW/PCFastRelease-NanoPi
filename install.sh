#!/bin/bash
#wget -O - https://gitlab.com/JonW/PCFastRelease-NanoPi/raw/master/install.sh | bash

DIRECTORY="/usr/local/bin/PCFastRelease"
systemctl stop PCFastRelease.service

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade PCFastRelease the latest version"
	echo "Uninstall current version"
	rm -rf $DIRECTORY
	rm /lib/systemd/system/PCFastRelease.service
	systemctl disable PCFastRelease.service
	systemctl daemon-reload
	
fi

echo "Install PCFastRelease"
mkdir $DIRECTORY
chmod 710 $DIRECTORY

git -C $DIRECTORY clone --recursive https://gitlab.com/JonW/PCFastRelease-NanoPi.git .

cp $DIRECTORY/PCFastRelease.service /lib/systemd/system/PCFastRelease.service

systemctl daemon-reload
systemctl enable PCFastRelease.service