#!/bin/bash
#wget -O - https://gitlab.com/JonW/PCFastRelease-NanoPi/raw/master/upgrade.sh | bash

DIRECTORY="/usr/local/bin/PCFastRelease"
systemctl stop PCFastRelease.service
systemctl disable PCFastRelease.service

BRANCH=${1:-master}

echo "Branch set to $BRANCH"

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade PCFastRelease the latest version"
	git -C $DIRECTORY fetch --all
	git -C $DIRECTORY reset --hard origin/$BRANCH
	git -C $DIRECTORY submodule foreach git reset --hard
	git -C $DIRECTORY submodule update --recursive
	
else
	echo "Install PCFastRelease"
	mkdir $DIRECTORY
	chmod 710 $DIRECTORY
	git -C $DIRECTORY clone --branch $BRANCH --recursive https://gitlab.com/JonW/PCFastRelease-NanoPi.git .
	
fi

cp $DIRECTORY/PCFastRelease.service /lib/systemd/system/PCFastRelease.service

systemctl daemon-reload
systemctl enable PCFastRelease.service
systemctl start PCFastRelease.service